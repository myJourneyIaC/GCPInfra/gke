# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A GKE PRIVATE CLUSTER IN GOOGLE CLOUD PLATFORM
# This is an prod of how to use the gke-cluster module to deploy a private Kubernetes cluster in GCP.
# Load Balancer in front of it.
# ---------------------------------------------------------------------------------------------------------------------

terraform {
  # The modules used in this prod have been updated with 0.12 syntax, additionally we depend on a bug fixed in
  # version 0.12.7.
  required_version = ">= 0.12.7"

  backend "gcs" {
    bucket = "tf-gke-bucket-state"
  }
  
}


# ---------------------------------------------------------------------------------------------------------------------
# PREPARE PROVIDERS
# ---------------------------------------------------------------------------------------------------------------------

provider "google" {
  version = "~> 3.28.0"
  project = var.project
  region  = var.region

  scopes = [
    # Default scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    "https://www.googleapis.com/auth/devstorage.full_control",

    # Required for google_client_openid_userinfo
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

provider "google-beta" {
  version = "~> 3.1.0"
  project = var.project
  region  = var.region

  scopes = [
    # Default scopes
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/ndev.clouddns.readwrite",
    "https://www.googleapis.com/auth/devstorage.full_control",

    # Required for google_client_openid_userinfo
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

# We use this data provider to expose an access token for communicating with the GKE cluster.
data "google_client_config" "client" {}

# Use this datasource to access the Terraform account's email for Kubernetes permissions.
data "google_client_openid_userinfo" "terraform_user" {}

provider "kubernetes" {
  version = "~> 1.11.0"

  load_config_file       = false
  host                   = data.template_file.gke_host_endpoint.rendered
  token                  = data.template_file.access_token.rendered
  cluster_ca_certificate = data.template_file.cluster_ca_certificate.rendered
}

provider "helm" {
  # Use provider with Helm 3.x support
  version = "~> 1.2.4"
  debug = true

  kubernetes {
    host                   = data.template_file.gke_host_endpoint.rendered
    token                  = data.template_file.access_token.rendered
    cluster_ca_certificate = data.template_file.cluster_ca_certificate.rendered
    load_config_file       = false
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A PRIVATE CLUSTER IN GOOGLE CLOUD PLATFORM
# ---------------------------------------------------------------------------------------------------------------------

module "gke_cluster" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following prod:
  # source = "github.com/gruntwork-io/terraform-google-gke.git//modules/gke-cluster?ref=v0.2.0"
  source = "./modules/gke-cluster"

  name = var.cluster_name

  project  = var.project
  location = var.location
  network  = module.vpc_network.network

  # Deploy the cluster in the 'private' subnetwork, outbound internet access will be provided by NAT
  # See the network access tier table for full details:
  # https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network#access-tier
  subnetwork = module.vpc_network.private_subnetwork

  # When creating a private cluster, the 'master_ipv4_cidr_block' has to be defined and the size must be /28
  master_ipv4_cidr_block = var.master_ipv4_cidr_block

  # This setting will make the cluster private
  enable_private_nodes = "true"

  # To make testing easier, we keep the public endpoint available. In production, we highly recommend restricting access to only within the network boundary, requiring your users to use a bastion host or VPN.
  disable_public_endpoint = "true"

  # With a private cluster, it is highly recommended to restrict access to the cluster master
  # However, for testing purposes we will allow all inbound traffic.
  master_authorized_networks_config = [
    {
      cidr_blocks = [
        {
          cidr_block   = "${google_compute_instance.kops-admin.network_interface.1.network_ip}/32"
          display_name = "all-for-gke-admin"
        },
      ]
    },
  ]
  enable_network_policy = "false"
  cluster_secondary_range_name  = module.vpc_network.private_subnetwork_cluster_range_name
  services_secondary_range_name = module.vpc_network.private_subnetwork_service_range_name
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE A NODE POOL
# ---------------------------------------------------------------------------------------------------------------------

resource "google_container_node_pool" "node_pool" {
  provider = google-beta

  name     = "main-pool"
  project  = var.project
  location = var.location
  cluster  = module.gke_cluster.name

  initial_node_count = "2"

  autoscaling {
    min_node_count = "1"
    max_node_count = "12"
  }

  management {
    auto_repair  = "true"
    auto_upgrade = "true"
  }

  node_config {
    image_type   = "COS_CONTAINERD"
    machine_type = "n1-standard-4"

    labels = {
      all-pools-prod = "true"
    }

    # Add a private tag to the instances. See the network access tier table for full details:
    # https://github.com/gruntwork-io/terraform-google-network/tree/master/modules/vpc-network#access-tier
    tags = [
      module.vpc_network.private,
      "helm-colmena",
    ]

    disk_size_gb = "30"
    disk_type    = "pd-standard"
    preemptible  = false

    service_account = module.gke_service_account.email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  lifecycle {
    ignore_changes = [initial_node_count]
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 0
  }

}


#----------------------------------------------------------------
# CILIUM
# https://docs.cilium.io/en/stable/gettingstarted/k8s-install-gke/
#----------------------------------------------------------------

resource "helm_release" "cilium" {

  depends_on = [google_container_node_pool.node_pool]
 
  name       = "cilium"
  chart      = "cilium"
  repository = "https://helm.cilium.io"
  namespace  = "kube-system"
  version    = "1.8.0"
  atomic     = true

  values = [
    yamlencode(
      {
        global = {
           cni = {
              binPath = "/home/kubernetes/bin"
              gke = "enabled" 
           }
           auto-direct-node-routes = true
           nativeRoutingCIDR =  cidrsubnet(
                                     var.pod_cidr_block,
                                     var.pod_cidr_subnetwork_width_delta,
                                     1 * (1 + var.pod_cidr_subnetwork_spacing)
                                 )

           enable-endpoint-routes = true
           blacklist-conflicting-routes = false
           enable-local-node-route = false
           masquerade = true
           enable-ipv4 = true
        }

        config  = { 
           ipam = "kubernetes"
        }
        nodeinit  = { 
           restartPods = true
           enabled = true
           reconfigureKubelet = true
           removeCbrBridge = true
        }
   })
   ]
}


# ---------------------------------------------------------------------------------------------------------------------
# CREATE A CUSTOM SERVICE ACCOUNT TO USE WITH THE GKE CLUSTER
# ---------------------------------------------------------------------------------------------------------------------

module "gke_service_account" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following prod:
  source = "git::https://gitlab.com/isapre-colmena-innovacion/InfraOps/CloudInfra/gke-svc-acct?ref=v1.0"

  name        = var.cluster_service_account_name
  project     = var.project
  description = var.cluster_service_account_description

  service_account_roles = [ "roles/owner",
                            "roles/monitoring.metricWriter",
                            "roles/monitoring.viewer",
                            "roles/stackdriver.resourceMetadata.writer",
                            "roles/iam.serviceAccountTokenCreator"
                          ]
}

/*
resource "google_project_iam_custom_role" "gke-custom-role" {
  project     = var.project 
  role_id     = "GkeServiceAccountCustomRole"
  title       = "GKE Service Account Custom Role"
  description = "GKE Runner Custom iam Role"
  permissions = [
    "iam.serviceAccounts.getTokenCreator"
  ]
}

resource "google_project_iam_binding" "gke-custom-binding" {
   project  = var.project 
   role     = google_project_iam_custom_role.gke-custom-role.id
   members  = [
      "serviceAccount:${module.gke_service_account.email}"
   ]
}

*/
# ---------------------------------------------------------------------------------------------------------------------
# CREATE A NETWORK TO DEPLOY THE CLUSTER TO
# ---------------------------------------------------------------------------------------------------------------------

resource "random_string" "suffix" {
  length  = 4
  special = false
  upper   = false
}

#  original source = "github.com/gruntwork-io/terraform-google-network.git//modules/vpc-network?ref=v0.4.0"
module "vpc_network" {

  source = "./modules/vpc-network"

  name_prefix = "${var.cluster_name}-network-${random_string.suffix.result}"
  project     = var.project
  region      = var.region
  domain      = var.domain-int

  cidr_block                            = var.vpc_cidr_block
  cidr_block_ilb                        = var.vpc_cidr_block_ilb
  cidr_subnetwork_width_delta           = var.cidr_subnetwork_width_delta
  secondary_cidr_block                  = var.vpc_secondary_cidr_block
  secondary_cidr_subnetwork_width_delta = var.secondary_cidr_subnetwork_width_delta

  pod_cidr_block                   = var.pod_cidr_block
  pod_cidr_subnetwork_width_delta  = var.pod_cidr_subnetwork_width_delta
  pod_cidr_subnetwork_spacing      = var.pod_cidr_subnetwork_spacing

  svc_cidr_block                   = var.svc_cidr_block
  svc_cidr_subnetwork_width_delta  = var.svc_cidr_subnetwork_width_delta
  svc_cidr_subnetwork_spacing      = var.svc_cidr_subnetwork_spacing

}

# ---------------------------------------------------------------------------------------------------------------------
# CONFIGURE KUBECTL AND RBAC ROLE PERMISSIONS
# ---------------------------------------------------------------------------------------------------------------------

# configure kubectl with the credentials of the GKE cluster
resource "null_resource" "configure_kubectl" {
  provisioner "local-exec" {
    command = "gcloud beta container clusters get-credentials ${module.gke_cluster.name} --region ${var.location} --project ${var.project}"

    # Use environment variables to allow custom kubectl config paths
    environment = {
      KUBECONFIG = var.kubectl_config_path != "" ? var.kubectl_config_path : ""
    }
  }

  depends_on = [google_container_node_pool.node_pool]
}

resource "kubernetes_cluster_role_binding" "user" {
  metadata {
    name = "admin-user"
  }

  role_ref {
    kind      = "ClusterRole"
    name      = "cluster-admin"
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "User"
    name      = data.google_client_openid_userinfo.terraform_user.email
    api_group = "rbac.authorization.k8s.io"
  }

  subject {
    kind      = "Group"
    name      = "system:masters"
    api_group = "rbac.authorization.k8s.io"
  }
}


# ---------------------------------------------------------------------------------------------------------------------
# K8S Addons
# ---------------------------------------------------------------------------------------------------------------------

module "k8s_addons" {

   source = "git::https://gitlab.com/isapre-colmena-innovacion/InfraOps/CloudInfra/K8s/k8s-add-ons.git?ref=v1.3.1"
   
   project                 = var.project
   location                = var.location
   domain                  = var.domain
   domain-int              = var.domain-int
   cluster_name            = var.cluster_name
   kubectl_config_path     = var.kubectl_config_path
   pod_cidr_block          = var.pod_cidr_block
   cfl-api-key             = var.cfl-api-key
   runnerRegistrationToken = var.runnerRegistrationToken
   region                  = var.region
   gitlab-api-token        = var.gitlab-api-token 
   instana-key             = var.instana-key
   network                 = module.vpc_network.network-id
   subnetwork              = module.vpc_network.ilb_private_subnetwork
   nodetags                = module.vpc_network.private
   nodeinstanceprefix      = var.nodeinstanceprefix

}


# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A SAMPLE CHART
# A chart repository is a location where packaged charts can be stored and shared. Define Bitnami Helm repository location,
# so Helm can install the nginx chart.
# ---------------------------------------------------------------------------------------------------------------------

resource "helm_release" "nginx" {
  depends_on = [google_container_node_pool.node_pool]

  repository = "https://charts.bitnami.com/bitnami"
  name       = "nginx"
  chart      = "nginx"
}

# ---------------------------------------------------------------------------------------------------------------------
# WORKAROUNDS
# ---------------------------------------------------------------------------------------------------------------------

# This is a workaround for the Kubernetes and Helm providers as Terraform doesn't currently support passing in module
# outputs to providers directly.
data "template_file" "gke_host_endpoint" {
  template = module.gke_cluster.endpoint
}

data "template_file" "access_token" {
  template = data.google_client_config.client.access_token
}

data "template_file" "cluster_ca_certificate" {
  template = module.gke_cluster.cluster_ca_certificate
}
