/*
// Image Base


resource "google_compute_instance" "vm-test" {
   name = "gke-test"
   machine_type = "e2-small"
   zone = "southamerica-east1-b"
   allow_stopping_for_update = true

   boot_disk {
     initialize_params {
       image = "ubuntu-os-cloud/ubuntu-1804-lts"
     }
   }

   metadata = {
    ssh-keys = join("\n", [for user, key in var.ssh_keys : "${key}"])
    name = "gke-admin"
    environment = "prod"
    ssh-server = "default-allow-ssh"
  }

  network_interface {
    network = "projects/colmena-cloud-gke/global/networks/colmena-prod-network"
    subnetwork = "colmena-prod-subnetwork-public"
     access_config {
    }
  }

}

output "ip-test" {
 value = google_compute_instance.vm-test.network_interface.0.access_config.0.nat_ip
}

*/
