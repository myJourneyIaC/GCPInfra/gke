
// Image Base

variable "ssh_keys" {
   description = "SSH Key for access"
   type = map
 }
 
variable "private_key_path" {
   description = "SSH Private Key"
 }

resource "google_compute_instance" "kops-admin" {
   name = "gke-admin"
   machine_type = "e2-small"
   zone = "southamerica-east1-b"
   allow_stopping_for_update = true

   boot_disk {
     initialize_params {
       image = "ubuntu-os-cloud/ubuntu-1804-lts"
     }
   }

   metadata = {
    ssh-keys = join("\n", [for user, key in var.ssh_keys : "${key}"])
    name = "gke-admin"
    environment = "prod"
    ssh-server = "default-allow-ssh"
  }


  network_interface {
    network = "default"
    subnetwork = "default"
     access_config {
    }
  }

  network_interface {
    network = module.vpc_network.network-id
    subnetwork = module.vpc_network.private_subnetwork_name
    network_ip = "10.141.0.21"
  }


  service_account { 
    email  = "gke-colmena-admin@colmena-cloud-gke.iam.gserviceaccount.com"
    scopes = [ "userinfo-email", 
               "compute-rw", 
               "storage-full",
               "cloud-platform",
               "datastore"
             ]
  }

  provisioner "remote-exec" {

    inline = [
      "echo cleanning apt pkg",
      "sudo apt-get clean",
      "echo updating apt pkg",
      "sudo apt-get update",
      "sudo apt-get install -y zip tree tmux python3 ipcalc traceroute",
      "echo upgrating apt",
      "sudo do-release-upgrade",
      "echo sleeping 30s give time between remote-exec and local-exec",
    ]

    connection {
      agent       = "false"
      host        = self.network_interface.0.access_config.0.nat_ip
     # host        = self.network_interface.0.network_ip
      type        = "ssh"
      user        = join("\n", [for user, key in var.ssh_keys : "${user}"])
      private_key = file(var.private_key_path)
    }
  }

  provisioner "local-exec" {
   # command = "sudo apt install python3-pip && sudo pip3 install ansible && ansible-playbook -u $USER --private-key ${var.private_key_path}  -i '${self.network_interface.0.network_ip},' custom.yml"
    command = "ansible-playbook -u $USER --private-key ${var.private_key_path}  -i '${self.network_interface.0.access_config.0.nat_ip},' custom.yml"
    environment = {
      USER = join("\n", [for user, key in var.ssh_keys : "${user}"])
      ANSIBLE_CONFIG = "${path.module}/files/ansible.cfg"
    }
  }
}

output "ip-int" {
 value = "${google_compute_instance.kops-admin.network_interface.1.network_ip}"
}

output "ip-ext" {
 value = google_compute_instance.kops-admin.network_interface.0.access_config.0.nat_ip
}
